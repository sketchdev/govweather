# Overview

The National Weather Service (NWS) offers an extensive API to retrieve current weather conditions and forecasts.
An overview can be found at https://www.weather.gov/documentation/services-web-api

Fetching weather information is bound to lat/long coordinates. For example, here are the coordinates for the
[St. Louis Zoo](https://goo.gl/maps/fJwGS9c9WcZ3vWnN7):

```
https://api.weather.gov/points/38.6359,-90.295
```

A subset of the response:
```json
{
    "properties": {
        "cwa": "LSX",
        "forecastOffice": "https://api.weather.gov/offices/LSX",
        "gridX": 91,
        "gridY": 74,
        "forecast": "https://api.weather.gov/gridpoints/LSX/91,74/forecast",
        "forecastHourly": "https://api.weather.gov/gridpoints/LSX/91,74/forecast/hourly",
        "forecastGridData": "https://api.weather.gov/gridpoints/LSX/91,74",
        "observationStations": "https://api.weather.gov/gridpoints/LSX/91,74/stations",
        "relativeLocation": {
            "properties": {
                "city": "Clayton",
                "state": "MO",
                "distance": {
                    "value": 3190.5755007487764,
                    "unitCode": "unit:m"
                }
            }
        },
        "forecastZone": "https://api.weather.gov/zones/forecast/MOZ064",
        "county": "https://api.weather.gov/zones/county/MOC510",
        "fireWeatherZone": "https://api.weather.gov/zones/fire/MOZ064",
        "timeZone": "America/Chicago",
        "radarStation": "KLSX"
    }
}
```

Weather is commonly retrieved using a [ZIP code](https://en.wikipedia.org/wiki/ZIP_Code). A mapping of ZIP codes to
lat/long coordinates can be found in
[`reference/zip.csv`](https://gitlab.com/sketchdev/govweather/blob/master/reference/zip.csv)

(the list was originally pulled from this gist: https://gist.github.com/erichurst/7882666)

# Current Weather Conditions

1. Fetch metadata from the `points` endpoint: `https://api.weather.gov/points/38.6359,-90.295`.

2. The current weather needs to be retrieved from an observation station. A list of stations is available by calling
the `observationsStations` URL returned from the endpoint in the previous step.

3. Use the first station returned in the array — it is sorted by distance (closest to furthest) from the coordinates
requested. Call the `https://api.weather.gov/stations/{stationId}/observations/latest` endpoint to fetch the current
conditions.

The latest observations response JSON returns various weather attributes (subset):
```json
{
  "properties": {
    "station": "https://api.weather.gov/stations/KSTL",
    "textDescription": "Mostly Cloudy",
    "icon": "https://api.weather.gov/icons/land/day/bkn?size=medium",
    "temperature": {
      "value": -4.3999999999999773,
      "unitCode": "unit:degC",
      "qualityControl": "qc:V"
    },
    "windChill": {
      "value": -9.768569895890721,
      "unitCode": "unit:degC",
      "qualityControl": "qc:V"
    }
  }
}

```

# Weather Forecast

The forecast is fetched in a similar way to the current weather conditions.

1. Fetch metadata from the `points` endpoint: `https://api.weather.gov/points/38.6359,-90.295`.

2. The forecast can be retrieved by calling the `forecast` URL found in the `points` response.

A subset of the forecast response JSON:
```json
{
  "properties": {
    "periods": [
      {
        "number": 1,
        "name": "Tonight",
        "startTime": "2019-12-04T22:00:00-06:00",
        "endTime": "2019-12-05T06:00:00-06:00",
        "isDaytime": false,
        "temperature": 34,
        "temperatureUnit": "F",
        "windSpeed": "3 mph",
        "windDirection": "SW",
        "icon": "https://api.weather.gov/icons/land/night/few?size=medium",
        "shortForecast": "Mostly Clear",
        "detailedForecast": "Mostly clear, with a low around 34. Southwest wind around 3 mph."
      },
      {
        "number": 2,
        "name": "Thursday",
        "startTime": "2019-12-05T06:00:00-06:00",
        "endTime": "2019-12-05T18:00:00-06:00",
        "isDaytime": true,
        "temperature": 55,
        "temperatureUnit": "F",
        "windSpeed": "3 to 7 mph",
        "windDirection": "S",
        "icon": "https://api.weather.gov/icons/land/day/bkn?size=medium",
        "shortForecast": "Partly Sunny",
        "detailedForecast": "Partly sunny, with a high near 55. South wind 3 to 7 mph."
      }
    ]
  }
}
```

# Weather Codes

A code representing the current condition can be found embedded in the `icon` field.
[`weathercode.go`](https://gitlab.com/sketchdev/govweather/blob/master/weathercode.go) parses the URL returning the
code as well as whether it is day or night. 

```go
govweather.CodeFromIconURL("https://api.weather.gov/icons/land/day/few?size=medium")
// few, true

govweather.CodeFromIconURL("https://api.weather.gov/icons/land/night/bkn?size=medium")
// bkn, false
```


A full list of codes and their associated descriptions can be retrieved from the `icons` endpoint:

```
curl -X GET "https://api.weather.gov/icons" -H  "accept: application/ld+json"
```

[`reference/icons.json`](https://gitlab.com/sketchdev/govweather/blob/master/reference/icons.json) is a copy of the
response. [`codes.go`](https://gitlab.com/sketchdev/govweather/blob/master/codes.go) constains constants representing
the codes.
