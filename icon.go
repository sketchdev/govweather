package govweather

var iconMap map[string][]string

func init() {
	iconMap = map[string][]string{
		CodeClear:                {"🌞", "🌚"},
		CodeFewClouds:            {"🌤", "🌙☁️"},
		CodePartyCloudy:          {"🌤", "🌙☁️"},
		CodeMostlyCloudy:         {"🌥", "🌙☁️"},
		CodeOvercast:             {"☁️", "☁️"},
		CodeClearAndWindy:        {"☀️💨", "☀️💨"},
		CodeFewCloudsAndWindy:    {"🌤💨", "🌙☁️💨"},
		CodePartlyCloudyAndWindy: {"🌤💨", "🌙☁️💨"},
		CodeMostlyCloudyAndWindy: {"🌥💨", "🌙☁️💨"},
		CodeOvercastAndWindy:     {"☁️💨", "☁️💨"},
		CodeSnow:                 {"❄️", "❄️"},
		CodeRainSnow:             {"☔️❄️", "☔️❄️"},
		CodeRainSleet:            {"☔️❄️", "☔️❄️"},
		CodeSnowSleet:            {"☔️❄️", "☔️❄️"},
		CodeFreezingRain:         {"☔️❄️", "☔️❄️"},
		CodeRainFreezingRain:     {"☔️❄️", "☔️❄️"},
		CodeFreezingRainSnow:     {"☔️❄️", "☔️❄️"},
		CodeSleet:                {"☔️❄️", "☔️❄️"},
		CodeRain:                 {"☔️", "☔️"},
		CodeRainShowers:          {"☔️", "☔️"},
		CodeRainShowersHi:        {"☔️", "☔️"},
		CodeThunderstorm:         {"⛈", "⛈"},
		CodeThunderstormMed:      {"⛈", "⛈"},
		CodeThunderstormHi:       {"⛈", "⛈"},
		CodeTornado:              {"🌪", "🌪"},
		CodeHurricane:            {"⛈💨", "⛈💨"},
		CodeTropicalStorm:        {"⛈💨", "⛈💨"},
		CodeDust:                 {"🌪", "🌪"},
		CodeSmoke:                {"💨", "💨"},
		CodeHaze:                 {"🌫", "🌫"},
		CodeFog:                  {"🌫", "🌫"},
		CodeHot:                  {"🔥", "🔥"},
		CodeCold:                 {"🥶", "🥶"},
		CodeBlizzard:             {"🌨", "🌨"},
	}
}
func WeatherIcon(code string, isDaytime bool) string {

	idx := func() int {
		if isDaytime {
			return 0
		}
		return 1
	}()

	if icon, ok := iconMap[code]; ok {
		return icon[idx]
	}
	return ""
}
