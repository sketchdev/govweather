package govweather

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCodeFromIconURL(t *testing.T) {

	t.Run("ensure valid URL is passed", func(t *testing.T) {

		code, isDaytime := CodeFromIconURL("https://sketchdev.io")

		assert.Empty(t, code, "if the URL is not of api.weather.gov/icons/ expecting an empty code")
		assert.False(t, isDaytime)
	})

	t.Run("incomplete URL returns empty code", func(t *testing.T) {

		code, isDaytime := CodeFromIconURL("https://api.weather.gov/icons/")

		assert.Empty(t, code)
		assert.False(t, isDaytime)
	})

	t.Run("code retrieved from path", func(t *testing.T) {

		for _, test := range []struct {
			IsDaytime bool
			Url       string
		}{
			{IsDaytime: true, Url: "https://api.weather.gov/icons/land/day/few?size=medium"},
			{IsDaytime: true, Url: "https://api.weather.gov/icons/land/day/few"},
			{IsDaytime: false, Url: "https://api.weather.gov/icons/land/night/few"},
		} {
			code, isDaytime := CodeFromIconURL(test.Url)

			assert.Equal(t, "few", code)
			assert.Equal(t, test.IsDaytime, isDaytime)
		}
	})

	t.Run("test code retrieve with percentages", func(t *testing.T) {

		url := "https://api.weather.gov/icons/land/day/rain,60/rain,50?size=medium"

		code, isDaytime := CodeFromIconURL(url)

		assert.Equal(t, "rain", code)
		assert.True(t, isDaytime)

		url = "https://api.weather.gov/icons/land/day/rain/few?size=medium"

		code, isDaytime = CodeFromIconURL(url)

		assert.Equal(t, "rain", code)
		assert.True(t, isDaytime)

	})
}
