package govweather

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWeatherIcon(t *testing.T) {

	t.Run("test valid code returns icon for day and night", func(t *testing.T) {

		assert.Equal(t, "🌞", WeatherIcon(CodeClear, true))
		assert.Equal(t, "🌚", WeatherIcon(CodeClear, false))

	})

	t.Run("test invalid code returns empty string", func(t *testing.T) {

		assert.Empty(t, WeatherIcon("non-existent", true))
		assert.Empty(t, WeatherIcon("non-existent", false))

	})
}
