package govweather

import "strings"

// CodeFromIconURL parses out the condition code from the icon URL
// see https://www.weather.gov/documentation/services-web-api#/default/get_icons__set___timeOfDay___first___second_
func CodeFromIconURL(urlStr string) (code string, isDaytime bool) {

	// example: https://api.weather.gov/icons/land/day/few?size=medium

	if !strings.Contains(urlStr, "/icons/") {
		return
	}

	// split the URL to get the path -> {"https://api.weather.gov", "land/day/few?size=medium"}
	urlComponents := strings.Split(urlStr, "/icons/")

	// decompose path -> {"land", "day", "few?size=medium"}
	if pathComponents := strings.Split(urlComponents[1], "/"); len(pathComponents) >= 3 {
		isDaytime = pathComponents[1] == "day"

		// decompose "few?size=medium" to get the code -> {"few", "size=medium"}
		if codeSplit := strings.Split(pathComponents[2], "?"); len(codeSplit) > 0 {
			code = codeSplit[0]

			// some URLs can have the path "land/day/rain,60/rain,50?size=medium"
			//   -> take the first code and filter out the numeric value
			if codeWithPercentage := strings.Split(code, ","); len(codeWithPercentage) >= 2 {
				code = codeWithPercentage[0]
			}
		}
	}
	return
}
